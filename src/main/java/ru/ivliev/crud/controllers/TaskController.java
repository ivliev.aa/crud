package ru.ivliev.crud.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ivliev.crud.dto.CommonDto;
import ru.ivliev.crud.dto.TaskDto;
import ru.ivliev.crud.model.Task;
import ru.ivliev.crud.services.TaskService;

import java.util.List;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping(value = "/task", produces = "application/json")
    public ResponseEntity createTask(@RequestBody TaskDto task) {
        taskService.create(task);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/task/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable("id") Long id) {
        TaskDto task = null;
        try {
            task = taskService.findById(id);
        } catch (ChangeSetPersister.NotFoundException e) {
            return (ResponseEntity<TaskDto>) ResponseEntity.notFound();
        }
        return new ResponseEntity(task, HttpStatus.FOUND);
    }

    @PutMapping("/task")
    public ResponseEntity changeTask(@RequestBody TaskDto taskDto) {
        try {
            taskService.change(taskDto);

        } catch (ChangeSetPersister.NotFoundException e) {
            return (ResponseEntity<TaskDto>) ResponseEntity.notFound();
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/task")
    public ResponseEntity deleteById(@RequestBody TaskDto taskDto) {
        try {
            taskService.delete(taskDto.getId());
        } catch (ChangeSetPersister.NotFoundException e) {
            return (ResponseEntity<TaskDto>) ResponseEntity.notFound();
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/task/list")
    public ResponseEntity<List<TaskDto>> list() {
        return new ResponseEntity(taskService.getAllTasks(), HttpStatus.FOUND);
    }

}
