package ru.ivliev.crud.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ivliev.crud.dto.TaskDto;
import ru.ivliev.crud.model.Task;
import ru.ivliev.crud.repo.TaskRepo;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    TaskRepo taskRepo;

    public void create(TaskDto taskDto) {
        Task task = convertToEntity(taskDto);

        task.setLastModification(new Date());
        taskRepo.save(task);
    }

    public TaskDto findById(Long id) throws ChangeSetPersister.NotFoundException {
        Optional<Task> optionalTask = taskRepo.findById(id);
        if (optionalTask.isPresent()) {
            return convertToDto(optionalTask.get());
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    @Transactional
    public void change(TaskDto taskDto) throws ChangeSetPersister.NotFoundException {

        Task task = convertToEntity(findById(taskDto.getId()));

        if (taskDto.getDescription() != null || !task.getDescription().equals(taskDto.getDescription())) {
            task.setDescription(taskDto.getDescription());
        }

        if (taskDto.getName() != null || !task.getName().equals(taskDto.getName())) {
            task.setName(taskDto.getName());
        }
        task.setLastModification(new Date());
    }

    public void delete(Long id) throws ChangeSetPersister.NotFoundException {
        Task task = convertToEntity(findById(id));
        taskRepo.delete(task);
    }

    public List<TaskDto> getAllTasks() {
        List<Task> list = (List<Task>) taskRepo.findAll();
        return list.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }


    private TaskDto convertToDto(Task task) {
        return new TaskDto(task.getId(), task.getName(), task.getDescription(), task.getLastModification());
    }

    private Task convertToEntity(TaskDto taskDto) {
        Task task = modelMapper.map(taskDto, Task.class);
        return task;
    }
}
