package ru.ivliev.crud.repo;


import org.springframework.data.repository.CrudRepository;
import ru.ivliev.crud.model.Task;

public interface TaskRepo extends CrudRepository<Task, Long> {

}
