package ru.ivliev.crud.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "TASKS")
public class Task {
    @GeneratedValue
    @Id
    Long id;

    String name;
    String description;
    Date lastModification;
}
